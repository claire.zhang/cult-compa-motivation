# cultural-compa-motivation

This folder holds the experiment programs we used in this paper: 
Zhang, Z., van Lieshout, L.L.F., Colizoli, O. et al. A cross-cultural comparison of intrinsic and extrinsic motivational drives for learning. Cogn Affect Behav Neurosci (2024). https://doi.org/10.3758/s13415-024-01228-2

# Description
searchlight-beijing-master is the program we used to test participants in Beijing, China. 
searchlight-dutch-master is the program we used to test participants in Nijmegen, the Netherlands. 

## Usage
To use these code, you need a python environment with the toolbox, pyppeteer. 

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
We will release the version we used in fMRI testing soon. 

## Authors and acknowledgment
We thank Wilbert van Ham from the technical support group in Radboud University for programming the experiment and consulting on preprocessing data.

## License
For this code, the GNU3.0 license applies. 

## Project status
This behavioral project has been published on Cognitive, Affective, and Behavioral Neuroscience. 